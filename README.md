# Ship Captain and Crew
Open source dice game - https://thealaskalinuxuser.wordpress.com

<a href="https://f-droid.org/packages/com.alaskalinuxuser.shipcaptainandcrew/" target="_blank">
<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80"/></a>

This app is lisenced under the Apache2.0 lisence. You are welcome to use this material for your own apps and purposes. This app was built in Android Studio for Android 4+.
